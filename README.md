# Chasevim
Vim for ME

run this program in order to install my vim configuration:
```bash
bash <(curl -s https://gitlab.com/chasedillard/chasevim-configuration/raw/master/install.sh)
```

Run this program in order to update the system:
```bash
bash <(curl -s https://gitlab.com/chasedillard/chasevim-configuration/raw/master/update.sh)
```

Run this program in order to install my bad vim configuration ;):
```bash
bash <(curl -s https://gitlab.com/chasedillard/chasevim-configuration/raw/master/install_for_losers.sh)
```

