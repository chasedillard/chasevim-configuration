#!/bin/bash

echo -e "WARNING!\nTHIS WILL WIPE YOUR PREVIOUS CONFIGURATION OF VIM!"
echo "I recommend backing up before you run this script."
echo "Would you like to continue? (Y/n)"

read userinput

if [[ $userinput == "n" ]] || [[ $userinput == "N" ]]; then
	exit 0
elif [[ $userinput == "Y" ]] || [[ $userinput == "y" ]] || [[ -z $userinput ]]; then
	git clone https://gitlab.com/chasedillard/chasevim-configuration.git $HOME/Chasevim
	rm -rf $HOME/.vim*
	
	mv $HOME/Chasevim/vimrc_for_losers $HOME/.vimrc
	mv $HOME/Chasevim/vim $HOME/.vim

	curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

	vim -c ":PlugInstall" -c ":qa"

	rm -rf $HOME/Chasevim
	. $HOME/.bashrc
	echo "DONE!"
	exit 0
else
	echo "I do not understand that command"
	exit 0
fi
