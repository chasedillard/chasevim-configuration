syntax on
colorscheme northpole
filetype plugin indent on
hi Normal     ctermbg=NONE guibg=NONE
hi LineNr     ctermbg=NONE guibg=NONE
hi SignColumn ctermbg=NONE guibg=NONE
hi Normal ctermbg=none
hi NonText ctermbg=none

set t_Co=256
set ignorecase
set backspace=indent,eol,start
set wildmenu
set nocompatible
set path+=**
set number relativenumber
set hlsearch
set encoding=utf-8
set ts=4 sts=0 sw=4 expandtab

autocmd FileType hs setlocal ts=3 sts=0 sw=3 expandtab

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
:nnoremap <silent> <CR> :nohlsearch<Bar>:echo<CR>

let g:tex_flavor='latex'
let java_highlight_functions = 1
hi Normal ctermbg=none
hi NonText ctermbg=none
hi String ctermbg=none

" Navigation Shortcuts

let mapleader = "\<Space>"
nnoremap ; :
nnoremap : ;
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-h> <C-w><C-h>
nnoremap <C-l> <C-w><C-l>

noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

nnoremap <leader>f :tabnext<CR>
nnoremap <leader>b :tabprevious<CR>


inoremap \<Tab> <Esc>/(<>)<Enter>"_c4l
vnoremap \<Tab> <Esc>/(<>)<Enter>"_c4l map <Space><Tab> <Esc>/(<>)<Enter>"_c4l
inoremap ;gui (<>)

inoremap jk <Esc>

" Python
nnoremap <f3> :w\|!python3 % <cr>
nnoremap <f8> :w\|!pep8 % -v <cr>

" TeX Shortcuts

" Linux Only: 

map <leader>o :!mupdf <C-r>%<Backspace><Backspace><Backspace>pdf<CR><CR>

" MacOS Only:

" :map <leader>o :!open -a Preview.app <C-r>%<Backspace><Backspace><Backspace>pdf<CR><CR>

:command Temptex :-1read ~/.vim/templates/template.tex
autocmd FileType tex inoremap ;bf \textbf{}(<>)<Esc>T{i
autocmd FileType tex inoremap ;em \emph{}(<>)<Esc>T{i
autocmd FileType tex inoremap ;abs \begin{abstract}<CR><CR>\end{abstract}(<>)<Esc>k
autocmd FileType tex inoremap ;sec \section{}<CR>(<>)<Esc>k_f{a
autocmd FileType tex inoremap ;ssec \subsection{}<CR>(<>)<Esc>k_f{a
autocmd FileType tex inoremap ;up \usepackage{}(<>)<Esc>_f{a
:autocmd FileType tex nnoremap <leader>c :w <bar> :!pdflatex %<CR><CR>
autocmd FileType tex inoremap ;vs \vspace{5mm}<CR>
autocmd FileType tex inoremap ;tab \begin{tabular}{(<>)}<CR>(<>)<CR>\end{tabular}<CR>(<>)
autocmd FileType tex inoremap ;goup <Esc>/usepackage<CR>o\usepackage{}i
autocmd Filetype tex nnoremap ;goup /usepackage<CR>o\usepackage{}i
autocmd FileType tex inoremap ;enum \begin{enumerate}<CR>(<>)<CR>\end{enumerate}<CR>(<>)
autocmd FileType tex inoremap ;item \begin{itemize}<CR>(<>)<CR>\end{itemize}<CR>(<>)

" Rust shortcuts

autocmd FileType rs setlocal ts=6 sw=6 noexpandtab
:command Temprs :read ~/.vim/templates/template.rs
autocmd FileType rs inoremap ;pl println!();(<>)<Esc>_f(a

" Assembly Shortcuts

:autocmd Filetype asm nnoremap <leader>c :w <bar> !nasm -f elf -o % <bar> !ld -o %<BS><BS><BS>o %<BS><BS><BS><BS><CR>

" C/C++ Shortcuts

:command Temph :-1read ~/.vim/templates/template.h
:command TempC :-1read ~/.vim/templates/template.c
:command Tempcpp :-1read ~/.vim/templates/template.cpp
autocmd Filetype cpp inoremap ;in #include<Space><>(<>)<Esc>_f<a
autocmd FileType cpp inoremap ;loin #include<Space>""(<>)<Esc>_f"a
:autocmd Filetype cpp nnoremap <leader>c :w <bar> !g++ %<CR>
:autocmd FileType cpp nnoremap <leader>m :!make && make install
autocmd Filetype c inoremap ;in #include<Space><>(<>)<Esc>_f<a
autocmd FileType c inoremap ;loin #include<Space>""(<>)<Esc>_f"a
:autocmd Filetype c nnoremap <leader>c :w <bar> !g++ %<CR>
:autocmd FileType c nnoremap <leader>m :!make && make install

" Java Shortcuts

autocmd Filetype java inoremap ;const public (<>)((<>)) {<CR>(<>)<CR>}<CR>(<>)<Esc>3k/(<>)<CR>c4l
autocmd Filetype java inoremap ;pl System.out.println();<CR>(<>)<Esc>k^f(a
:command Tempjava :-1read ~/.vim/templates/template.java

" SQL
autocmd FileType sql setlocal tabstop=8 shiftwidth=8 expandtab

nnoremap <leader>e :call FzyCommand("find -type f", ":e")<CR>
nnoremap <leader>v :call FzyCommand("find -type f", ":vs")<CR>
nnoremap <leader>s :call FzyCommand("find -type f", ":sp")<CR>

" Ruby

autocmd FileType ruby setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType eruby setlocal expandtab shiftwidth=2 tabstop=2



" Vim-plug

call plug#begin('~/.vim/plugged')
Plug 'ervandew/supertab'
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
Plug 'mattn/calendar-vim'
Plug 'vim-scripts/utl.vim'
Plug 'chrisbra/NrrwRgn'
Plug 'vim-scripts/SyntaxRange'
Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'
Plug 'tpope/vim-surround'
Plug 'w0rp/ale'
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-abolish'
Plug 'editorconfig/editorconfig-vim'
Plug 'sjl/gundo.vim'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'devjoe/vim-codequery'
Plug 'itchyny/vim-haskell-indent'
Plug 'wlangstroth/vim-racket'
Plug 'kongo2002/fsharp-vim'
Plug 'elixir-editors/vim-elixir'
Plug 'NLKNguyen/papercolor-theme'
Plug 'vim-ruby/vim-ruby'
Plug 'tpope/vim-rails'
Plug 'scrooloose/nerdtree'
call plug#end()

" Lightline Settings

set laststatus=2

" Noshwmode
set noshowmode

" General Runtime Things
:if  $MYCOLORSCHEME == 1
:    set background=light
:    colorscheme PaperColor
     let g:lightline = {
         \ 'colorscheme': 'PaperColor'
         \ }
:endif
